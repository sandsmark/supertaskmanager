/***************************************************************************
 *   Copyright (C) 2012-2013 by Eike Hein <hein@kde.org>                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

function horizontalMargins() {
    return taskFrame.margins.left + taskFrame.margins.right;
}

function verticalMargins() {
    return taskFrame.margins.top + taskFrame.margins.bottom;
}

function logicalTaskCount() {
    var count = tasks.vertical ? tasksModel.count : tasksModel.count - tasksModel.launcherCount;

    return Math.max(tasksModel.count ? 1 : 0, count);
}

function maxStripes() {
    var length = tasks.vertical ? taskList.width : taskList.height;
    var minimum = tasks.vertical ? preferredMinWidth() : preferredMinHeight();

    return Math.min(tasks.maxStripes, Math.max(1, Math.floor(length / minimum)));
}

function tasksPerStripe() {
    if (tasks.forceStripes) {
        return Math.ceil(logicalTaskCount() / maxStripes());
    } else {
        var length = tasks.vertical ? taskList.height : taskList.width;
        var minimum = tasks.vertical ? preferredMinHeight() : preferredMinWidth();

        return Math.floor(length / minimum);
    }
}

function calculateStripes() {
    var stripes = tasks.forceStripes ? tasks.maxStripes : Math.min(tasks.maxStripes, Math.ceil(logicalTaskCount() / tasksPerStripe()));

    return Math.min(stripes, maxStripes());
}

function full() {
    return (maxStripes() == calculateStripes());
}

function layoutWidth() {
    if (tasks.forceStripes && !tasks.vertical) {
        return Math.min(tasks.width, Math.max(preferredMaxWidth(), tasksPerStripe() * preferredMaxWidth() + Math.ceil(tasksModel.launcherCount / maxStripes()) * launcherWidth()));
    } else {
        return tasks.width;
    }
}

function layoutHeight() {
    if (tasks.forceStripes && tasks.vertical) {
        return Math.min(tasks.height, Math.max(preferredMaxHeight(), tasksPerStripe() * preferredMaxHeight()));
    } else {
        return tasks.height;
    }
}

function preferredMinWidth() {
    if (tasks.vertical) {
        return horizontalMargins() + theme.smallIconSize;
    } else {
        return horizontalMargins() + theme.smallIconSize + 3 + (mWidth * 13);
    }
}

function preferredMaxWidth() {
    return preferredMinWidth();
}

function preferredMinHeight() {
    // TODO: Port to proper font metrics for descenders once we have access to them.
    return mHeight + 4;
}

function preferredMaxHeight() {
    return verticalMargins() + Math.min(theme.smallIconSize * 3, mHeight * 3);
}

function taskWidth() {
    if (tasks.vertical) {
        return Math.floor(taskList.width / calculateStripes());
    } else {
        if (full() && Math.max(1, logicalTaskCount()) > tasksPerStripe()) {
            return Math.floor(taskList.width / Math.ceil(logicalTaskCount() / maxStripes()));
        } else {
            return Math.min(preferredMaxWidth(), Math.floor(taskList.width / Math.min(logicalTaskCount(), tasksPerStripe())));
        }
    }
}

function taskHeight() {
    if (tasks.vertical) {
        if (full() && Math.max(1, logicalTaskCount()) > tasksPerStripe()) {
            return Math.floor(taskList.height / Math.ceil(logicalTaskCount() / maxStripes()));
        } else {
            return Math.min(preferredMaxHeight(), Math.floor(taskList.height / Math.min(logicalTaskCount(), tasksPerStripe())));
        }
    } else {
        return Math.floor(taskList.height / calculateStripes());
    }
}

function launcherWidth() {
    return taskHeight();
    var height = taskHeight();

    if ((height % 2) == 1) {
        return height + 1;
    }

    return height;
}

function layout(container) {
    if (tasksModel.count == 0) {
        return;
    }

    var width = taskWidth();
    var height = taskHeight();
    var lWidth = launcherWidth();
    var pMaxWidth = preferredMaxWidth();
    var perStripe = Math.floor(taskList.width / width);

    var item;
    var task = 0;
    var launcherCountForStripe = new Array();
    var taskCountForStripe = new Array();
    var launcherCount = 0;
    var taskCount = 0;

    for (var i = 0; i < container.count; ++i) {
        item = container.itemAt(i);

        if (!item) {
            return;
        }

        if (item.isLauncher) {
            ++launcherCount;
        } else {
            ++task;
            ++taskCount;
        }

        if (task == perStripe) {
            task = 0;
            launcherCountForStripe.push(launcherCount);
            taskCountForStripe.push(taskCount);
            launcherCount = 0;
            taskCount = 0;
        }
    }

    launcherCountForStripe.push(launcherCount);
    taskCountForStripe.push(taskCount);

    var stripe = 0;
    task = 0;
    launcherCount = (calculateStripes() > 1) ? launcherCountForStripe[stripe] : tasksModel.launcherCount;
    taskCount = taskCountForStripe[stripe];

    var diff = Math.ceil((launcherCount * lWidth) / taskCount);
    var volume = (taskCount * (width - diff)) + (launcherCount * lWidth);
    diff = diff - Math.max(0, Math.floor((taskList.width - volume) / taskCount));

    for (var i = 0; i < container.count; ++i) {
        item = container.itemAt(i);
        item.visible = true;

        if (!tasks.vertical) {
            if (item.isLauncher) {
                item.width = lWidth;
            } else {
                ++task;
                item.width = Math.min(pMaxWidth, width - diff);
            }

            if (task == perStripe) {
                ++stripe;
                task = 0;

                if (launcherCountForStripe[stripe] != launcherCount) {
                    launcherCount = launcherCountForStripe[stripe];
                    taskCount = taskCountForStripe[stripe];
                    diff = Math.ceil((launcherCount * lWidth) / taskCount);
                    volume = (taskCount * (width - diff)) + (launcherCount * lWidth);
                    diff = diff - Math.max(0, Math.floor((taskList.width - volume) / taskCount));
                }
            }
        } else {
            item.width = Math.min(pMaxWidth, width - diff);
        }

        item.height = height;
    }
}
