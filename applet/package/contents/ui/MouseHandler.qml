/***************************************************************************
 *   Copyright (C) 2012-2013 by Eike Hein <hein@kde.org>                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

import QtQuick 1.1

import org.kde.draganddrop 1.0
import org.kde.qtextracomponents 0.1

import "../code/layout.js" as Layout
import "../code/tools.js" as TaskTools

Item {
    property Item target
    property Item ignoreItem
    property bool moved: false

    DropArea {
        id: dropHandler

        anchors.fill: parent

        property Item hoveredItem

        onDragMove: {
            var above = target.childAt(event.x, event.y);

            if (moved) {
                ignoreItem = above;

                if (!target.animating) {
                    moved = false;
                }
            }

            if (target.animating) {
                return;
            }

            if (ignoreItem && above != ignoreItem) {
                ignoreItem = null;
            }

            if (tasks.dragSource) {
                if (tasks.dragSource != above && above != ignoreItem) {
                    itemMove(tasks.dragSource.itemId,
                        TaskTools.insertionIndexAt(tasks.dragSource.itemIndex,
                            event.x, event.y));
                    moved = true;
                }
            } else if (above && hoveredItem != above) {
                hoveredItem = above;
                activationTimer.start();
            } else if (!above) {
                hoveredItem = null;
                activationTimer.stop();
            }
        }

        onDragLeave: {
            hoveredItem = null;
            activationTimer.stop();
        }

        Timer {
            id: activationTimer

            interval: 250
            repeat: false

            onTriggered: {
                if (!parent.hoveredItem.isLauncher) {
                    tasks.activateItem(parent.hoveredItem.itemId, false);
                }
            }
        }
    }

    MouseEventListener {
        id: wheelHandler

        anchors.fill: parent

        onWheelMoved: TaskTools.activateNextPrevTask(false, wheel.delta < 0)
    }
}
